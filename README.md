### Сущность рекламная компания (ads_campaign)
Связана с:

* видеозаписью (video) - видеозапись которая показывается на мониторе во время рекламной компании
* рекламодателем (advertiser) - рекламодатель который заказал данную реклмамную компанию
* партнёром (partner) - партнёр который проводит данную рекламную компанию
* пользователем (user) который её создал
* пользователем (user) который её завершил
* финансовыми операциями (finance_operation)
* данными о показах за определенный час дня (show_count_dayhour) - данной рекламной компании
* данными о показах за неделю (show_count_weekday) - данной рекламной компании
* данными о показах за период (show_count_period) - данной рекламной компании
* данные о показах по GPS - Данные о показах по координатам GPS
* отключенный некоммерческий контент по подписке (noncommercial_content) - данной рекламной компании 
* данные о области полигонов (ads_campaign_polygon_area) - данной рекламной компании
* данные о области рекламной компании (ads_campaign_area) - области которая принадлежит данной рекламной компании
* группа о данных рекламной компании (ads_campaign_group)

### Сущность партнёр (partner) - партнёр который проводит рекламной компанию
Связана с:

* пользователем (user) -  пользователь который создал данную рекламную компанию
* группой города (group_city) - группа города к который относится партнёр
* валютой (currency) - используемая данным партнером валюта для расчетов
* тарифом (tariff) - тариф по которому работает партнёр
* партнёр по экстренной помощи (emergency_partner) - партнёр ответственный за экстренную помощь
* областями станций (depot_area)
* отключенными контентами по подписке (disabled_subscription_content) - контентом по подписке относящимуся к данному партнёру
* рекламодателями (advertiser) - рекламодатели данного партнёра
* группами партнёров (partner_group) - группы в которые входит данный партнёр
* некоммерческими контентами по подписке (noncommercial_content) - некоммерческий контент который относится к даннмоу партнёру
* пользователями (partner_user) - пользователи которые относятся к данному партнёру
* остановками транспорта (transport_stop) - остановками транспорта
* виджетами(widget) - данного партнёра

### Сущность монитор (monitor) - монитор на котором показывается рекламная компания
Связана с:

* партнёром (partner) - партнёр который отвечает за данный монитор
* группой монитора (monitor_group) - группа к которой относится данный монитор
* маршрутом (route) - марштрут транспорта на котором установлен монитор
* опубликованным файлом (posted_file) - опубликованным файлом для данного монитора
* история рабочего времени монитора (monitor_working_time_history) - записанное рабочее время данного монитора
* командами для данного монитора (monitor_command)
* временная шкала истории работы монитора (monitor_timeline_history)
* коды итоговых данных монитора (monitor_code_total)

### Сущность область станции (depot_area)- область станции закрепленная за данными партнёром
Связана с:

* партнёром (partner) - партнёр который закреплен за данной станцией

### Сущность отключенный контент по подписке - контент подписка по которому была прекращена
Связана с:

* некоммерческий контент (noncommercial_content) - некоммерческий контент подписка на который была прекращена
* партнёр (partner) - связанный с контентом партнёр

### Сущность сообщение экстренной помощи (emergency_message)
Связана с:

* шаблон сообщения экстренной помощи (emergency_message) - шаблон по которому создано сообщение экстренной помощи
* картинка экстренной помощи (image) - картинка титула сообщения экстренной помощи
* пользователь (user) - пользователь отправивший сообщение экстренной помощи
* расширенным сообщением экстренной помощи (emergency_message_advanced)
* область экстренной помощи (emergency_message_area) - область в которой данное сообщение активно
* областями полигонов (emergency_message_polygon_area) - область полигонов в которой данное сообщение активно
* данные о количестве показов данного сообщения за неделю (emergency_message_show_count_weekday)
* данные о количестве показов данного сообщения за час/день (emergency_message_show_count_dayhour)
* данные о количестве показов данного сообщения за период (emergency_message_show_count_period)

### Сущность картинка (image) - графический файл в файловом хранилище
Связана с:

* сотрудником (user) - сотрудник который добавил картинку
* сообщением экстренной помощи (emergency_message)
* шаблон сообщения экстренной помощи (emergency_message) - шаблон по которому создано сообщение экстренной помощи
* расширенным сообщением экстренной помощи (emergency_message_advanced)

### Сущность группа города (group_city) - группировка по городу для различных данных (см связи ниже)
Связана с:

* областью сообщения экстренной помощи (emergency_message_area) - область на которой должна показываться сообщение экстренной помощи
* областью полигонов сообщения экстренной помощи (emergency_message_area) - область полигонов сообщения экстренной помощи
* количеством показов за день/час (emergency_message_show_count_dayhour) - данные о количестве показов сообщения экстренной помощи в данной группе за определенные часы дня
* количеством показов за период (emergency_message_show_count_period) - данные о количестве показов сообщения экстренной помощи в данной группе за определенный период
* партнёром (partner) - партнёром который входит в данную группу города
* данным количеством показов за день/час рекламной компании в день в определенные часы (show_count_dayhour) - данные о количестве показов рекламной компании за день/час
* группой партнёра (partner_group) - с соответствующей группой в которую входит данный партнёра
* область рекламной компании (ads_campaign_area) - область в которой рекламная компания должна быть активна
* область полигонов рекламной компании (ads_campaign_polygon_area) - описание области в которой рекламная компания должна быть активна в виде полигонов
* городом закрепленному за МЧС (mcs_city) 
* монитором (monitor) - монитор который входит в данную группу города
* количество данных показа за неделю (show_count_weekday) - количество данных о показах за неделю относящихся к данной группе
* данными о прогнозе погоды (weather_forecast) - данные о прогнозе погоды в данной группе города

### Сущность данные прогноза погоды (weather_forecast)
Связана с:

* городом (city)

### Сущности не имеющие связей

* Данные о версии системы (VersionInfo)
* Данные о версии архивов APK (apk_version)
* Данные о курсах обмена валют (exchange_rate)
* Данные о ограничениях по IP для МЧС (mcs_ip_restrictions)

### Сущность данные для мчс (mcs)

Связана с:

* Данными города для МЧС (mcs_city)


### Сущность пользователи (user) - пользователь зарегистрированный в системе

Связана с:

* кодом подтверждения (confirmation_code) - Код подтверждения при регистрации пользователя
* пользователем который его создал (user)
* подтверждениями токена пользователя (tokens_email_confirm)
* маршрутом (route) - маршруты созданные пользователем
* пользователь (сотрудник) экстренной помощи  (emergency_user) - сотрудник отвечающий за экстренную помощь
* сообщениями о экстренной помощи (emergency_message) 
* командами к монитору - созданными данным пользователем (monitor_command)
* фотографией данного пользователя (image)
* сотрудником партнёра (partner_user)
* сотрудником рекламодателя (advertiser_user)
* рекламодатель (advertiser)
* Финансовыми операциями -  созданными данным пользователем (finance_operation)
* Рекламной компанией (ads_campaign) - которая создана/завершена данным пользователем

